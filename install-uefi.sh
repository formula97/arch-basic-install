#!/bin/bash

ln -sf /usr/share/zoneinfo/US/Central /etc/localtime
hwclock --systohc
sed -i '177s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "ArchSP4" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 ArchSP4.grasse.family ArchSP4" >> /etc/hosts

# You can add xorg to the installation packages, I usually add it at the DE or WM install script
# You can remove the tlp package if you are installing on a desktop or vm

pacman -S grub grub-btrfs avahi acpid efibootmgr networkmanager git nano wpa_supplicant base-devel linux-headers rsync bluez bluez-utils pipewire bash-completion reflector tlp sudo

# pacman -S --noconfirm xf86-video-amdgpu
# pacman -S --noconfirm nvidia nvidia-utils nvidia-settings

grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable avahi-daemon
systemctl enable tlp # You can comment this command out if you didn't install tlp, see above
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable acpid

useradd -m formula97
echo formula97:password | chpasswd

echo "formula97 ALL=(ALL) ALL" >> /etc/sudoers.d/formula97

usermod -p '!' root

printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"
