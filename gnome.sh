#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc

# sudo reflector -c 'United States' -a 12 --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -Sy

# sudo virsh net-autostart default

sudo pacman -S --noconfirm gdm gnome-session gnome-tweaks gnome-keyring archlinux-wallpaper evolution gnome-control-center gnome-disk-utility gnome-menus gnome-settings-daemon gnome-system-monitor dialog gvfs gvfs-nfs gvfs-smb xdg-user-dirs-gtk nautilus terminator arc-gtk-theme arc-icon-theme udisks2

sudo systemctl enable gdm
/bin/echo -e "\e[1;32mREBOOTING IN 5..4..3..2..1..\e[0m"
sleep 5
sudo reboot
